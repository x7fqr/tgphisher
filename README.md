# tgPhisher

This is a simple telegram bot, to get a phone number of the user and save it to your database
## Installation (Linux/Termux/Windows)

```bash
  git clone https://0xacab.org/farblose/tgphisher.git
  cd tgphisher
  pip install -r requirements.txt
```
    
## Usage

First of all you need to dm @BotFather on telegram and create your bot.
Then you need to copy your bot token and paste it in setting.py file
```python
#settings.py
TOKEN = "Your token here"
```
Now you can start the bot
```bash
python main.py
```