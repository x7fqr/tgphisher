import sqlite3 as sq

class database:
    def __init__(self, db_file):
        self.db = sq.connect(db_file)
        self.cur = self.db.cursor()
        self.cur.execute("CREATE TABLE IF NOT EXISTS users(user_id INTEGER PRIMARY KEY, phone TEXT)")
        self.db.commit()

    def create_user(self, user_id, phone):
        self.cur.execute(f"SELECT 1 FROM users WHERE user_id == {user_id}")
        user = self.cur.fetchone()
        if not user:
            self.cur.execute("INSERT INTO users VALUES(?, ?)", (user_id, phone))
            self.db.commit()
